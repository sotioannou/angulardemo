import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { ADD_ITEMS, ADD_PHOTOS, ADD_SEARCH_TEXT } from './reducers/main.reducer';
import { AppState, PostObject, ImageObject } from './reducers/json-objects';

@Injectable()
export class DataRequestService {
  url: String = 'https://jsonplaceholder.typicode.com/';
  picturesUrl: String = 'https://pixabay.com/api/';
  picturesAPIKey: String = '6494743-7947e11d8ea5a4e4b9ad1bb25';

  // store Observables
  items: Observable <PostObject[]>;
  photos: Observable <ImageObject[]>;
  searchTextObservable: Observable <string>;

  constructor(private http: HttpClient, private store: Store<AppState>) {
    // subscribe to observables
    this.items = store.select('items');
    this.photos = store.select('photos');
    this.searchTextObservable = store.select('searchText');
    this.fetchPosts();
    this.fetchPhotos();
  }
  fetchPosts(): void {
    this.http.get(this.url + 'posts').subscribe(response => {
      this.store.dispatch({ type: 'ADD_ITEMS', payload: response});
    });
  }
  fetchPhotos(): void {
    this.http.get(this.picturesUrl + '?key=' + this.picturesAPIKey + '&q=dogs').subscribe(response => {
      this.store.dispatch({ type: 'ADD_PHOTOS', payload: response['hits']});
    });
  }
  getStoreSearchText(): Observable <string> {
    return this.searchTextObservable;
  }
  storeSearchText(text): Observable <string> {
    this.store.dispatch({ type: 'ADD_SEARCH_TEXT', payload: text});
    return this.searchTextObservable;
  }
}
