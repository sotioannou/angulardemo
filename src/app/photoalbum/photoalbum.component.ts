import { Component, OnInit } from '@angular/core';
// services
import { DataRequestService } from '../data-request.service';
// classes
import { Image } from '../image';
@Component({
  selector: 'app-photoalbum',
  templateUrl: './photoalbum.component.html',
  styleUrls: ['./photoalbum.component.css']
})
export class PhotoalbumComponent implements OnInit {
  photosList = [];
  constructor(private dataService: DataRequestService) { }

  ngOnInit() {
       this.dataService.photos.subscribe(data => {
         if (data['photos'].length > 0) {
            this.prepareObjects(data['photos']);
         }
    });
  }
  prepareObjects(data) {
    this.photosList = data.map((entry) => {
      // remove white space
      let tmpArray: string[] = entry.tags.split(",").map(item => {return item.trim();});
      return new Image(entry.previewURL, tmpArray, entry.previewWidth, entry.previewHeight);
    });
  }
  imageClicked(image) {
    console.log(image);
  }
}
