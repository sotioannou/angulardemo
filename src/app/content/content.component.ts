import { Component, OnInit, Input } from '@angular/core';
// services
import { DataRequestService } from '../data-request.service';
import { PostObject } from '../reducers/json-objects';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
    @Input() searchQuery: string;
    results: PostObject[];
    results_filtered: Object;
    testArray: PostObject[];
    loading: boolean;

    constructor(private dataService: DataRequestService) {
        this.loading = true;
    }
    ngOnInit() {
      this.dataService.items.subscribe(data => {
      if (data['items'].length > 0) {
          this.loading = false;
          // console.log(data);
         this.results = data['items'];
      }
       });
    }
}
