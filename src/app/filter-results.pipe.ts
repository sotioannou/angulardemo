import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterResults'
})
export class FilterResultsPipe implements PipeTransform {

    transform(items: any, searchId: string): any {
        if (searchId === undefined || searchId === null) { return items; }
        const itemsFiltered = items.filter((entry) => {
            return entry.title.toLowerCase() === searchId.toLowerCase();
          //  return val.id === parseInt(searchId);
        });
        if (itemsFiltered.length > 0) {
            return itemsFiltered;
        } else { return items; }
    }
}
