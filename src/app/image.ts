export class Image {
    constructor( 
    	public imageSrc: string,  
		public name: Array<string>,
		public width: number,
		public height: number
    ){}
}
