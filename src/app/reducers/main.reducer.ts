import { Action, State } from '@ngrx/store';
import { AppState, PostObject, ImageObject } from '../reducers/json-objects';


// create CustomAction to solve problem with the payload
export interface CustomAction extends Action {
    type: string;
    payload?: any;
}

export const ADD_ITEMS = 'ADD_ITEMS';
export const ADD_PHOTOS = 'ADD_PHOTOS';
export const ADD_SEARCH_TEXT = 'ADD_SEARCH_TEXT';

export function mainReducer(state: AppState = {items: [], photos: [], searchText: ''}, action: CustomAction) {
	switch (action.type) {
        case ADD_ITEMS:
           return {
            items: [...state.items, ...action.payload],
            photos: [...state.photos, ...[]],
            searchText: state.searchText
        };
        case ADD_PHOTOS:
             return {
                items: [...state.items, ...[]],
                photos: [...state.photos, ...action.payload],
                searchText: state.searchText
            };
            case ADD_SEARCH_TEXT:
                return {
                    items: [...state.items, ...[]],
                    photos: [...state.photos, ...[]],
                    searchText: action.payload
                };
		default:
			return state;
	}
}