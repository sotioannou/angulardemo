export interface AppState {
    items: PostObject[];
    photos: ImageObject[];
    searchText: string;
  }

export interface PostObject {
    id: number;
    userId: number;
    title: string;
    body: string;
}
export interface ImageObject {
    comments: number;
    downloads: number;
    favorites: number;
    id: number;
    imageHeight: number;
    imageWidth: number;
    likes: number;
    pageURL: string;
    previewHeight: number;
    previewURL: string;
    previewWidth: number;
    tags: string;
    type: string;
    user: string;
    userImageURL: string;
    user_id: number;
    views: number;
    webformatHeight: number;
    webformatURL: string;
    webformatWidth: number;
}
