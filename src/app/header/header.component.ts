import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataRequestService } from '../data-request.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    @Input() title: string;
    @Input() storedValue: string;
    @Output() text: EventEmitter<string>;

    constructor(private dataService: DataRequestService) {
        this.text = new EventEmitter();
    }

    ngOnInit() {
        // check if we have searchbox text in store already
        this.dataService.getStoreSearchText().subscribe(data => {
            if (data['searchText'] !== '' ) {
                this.storedValue = data['searchText'];
                this.text.emit(data['searchText']);
            }
        });
    }

    onEnter(searchbox) {
        this.text.emit(searchbox.value);
        this.dataService.storeSearchText(searchbox.value);
    }
}
