import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MainComponent } from './main/main.component';
import { ContentComponent } from './content/content.component';
import { FilterResultsPipe } from './filter-results.pipe';

// services
import { DataRequestService } from './data-request.service';
import { NavigationComponent } from './navigation/navigation.component';
import { PhotoalbumComponent } from './photoalbum/photoalbum.component';

// state
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { mainReducer } from './reducers/main.reducer';

const appRoutes: Routes = [
    {path: '', component: MainComponent},
    {path: 'photoalbum', component: PhotoalbumComponent},
    {path: '**', component: NotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NotFoundComponent,
    MainComponent,
    ContentComponent,
    FilterResultsPipe,
    NavigationComponent,
    PhotoalbumComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({ items: mainReducer, photos: mainReducer, searchText: mainReducer }),
    StoreDevtoolsModule.instrument({
      maxAge: 25 //  Retains last 25 states
    }),
      HttpClientModule,
      RouterModule.forRoot(
      appRoutes,
      // remove for production
      {enableTracing: false}
    ),
  ],
  providers: [DataRequestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
